d3.csv("T10.csv", function(data) { //load callback function
	var canvas = d3.select("body").append("svg")
		.attr("width", 500)
		.attr("height", 500);

	canvas.selectAll("rect")
		.data(data)
		.enter()
		.append("rect")
		.attr("width", function(d) {
			return d.age * 10;
		})
		.attr("height", 30)
		.attr("y", function(d,i){
			return i * 50;
		})
		.attr("fill", "blue");

})