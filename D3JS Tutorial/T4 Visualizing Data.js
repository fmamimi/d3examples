//bar
//scale

var dataArray = [20, 40, 50, 60]; //domain

var width = 500;
var height = 500;

/////////////////////scale: linear///////////////////////
var widthScale = d3.scale.linear()
  .domain([0, 60]) //the original range
  .range([0, width]); //the mapped range
/////////////////////scale: linear///////////////////////

var color = d3.scale.linear()
  .domain([0, 60])
  .range(["red", "blue"]);

/////////////////////axis///////////////////////
var axis = d3.svg.axis()
  .ticks(5)
  .scale(widthScale);
/////////////////////axis///////////////////////

/////////////////////group///////////////////////
var canvas = d3.select("body")
  .append("svg")
  .attr("width", width)
  .attr("height", height)
  .append("g") //group
  .attr("transform", "translate(20,0)");
//  .call(axis);
/////////////////////group///////////////////////


/////////////////////function in attr///////////////////////
var bars = canvas.selectAll("rect") //if there's no element, it will return empty
  .data(dataArray) //where the data comes from
  .enter() //contains placeholders for EACH data element
  .append("rect")
  .attr("width", function(d) { //d: each data element
    return widthScale(d); //return the data as width
  })
  .attr("height", 50)
  .attr("fill", function(d) {
    return color(d)
  })
  .attr("y", function(d, i) {
    return i * 100
  }); //attr function param: 1st: data; 2nd: index
/////////////////////function in attr///////////////////////

canvas.append("g")
  .attr("transform", "translate(0,400)")
  .call(axis);