var data = [10];

var canvas = d3.select("body")
	.append("svg")
	.attr("width", 500)
	.attr("height", 500)

var circle1 = canvas.append("circle")
	.attr("cx", 50)
	.attr("cy", 50)
	.attr("r", 25);

var circle2 = canvas.append("circle")
	.attr("cx", 50)
	.attr("cy", 200)
	.attr("r", 25);

////////////////////////exit: DOM > data/////////////////
var circles = canvas.selectAll("circle")
	.data(data)
	.attr("fill", "red") //Update selection
	.exit()
	.attr("fill","blue");
////////////////////////exit: DOM > data/////////////////

////////////////////////enter: DOM < data/////////////////
	// .enter() //add more DOM elements if DOM < data
	// .append("circle")
	// .attr("cx", 100)
	// .attr("cy", 50)
	// .attr("fill", "green")
	// .attr("r", 25);
////////////////////////enter: DOM < data/////////////////