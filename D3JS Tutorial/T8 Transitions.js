//transition
//each method

var canvas = d3.select("body")
	.append("svg")
	.attr("width", 500)
	.attr("height", 500)

var circle = canvas.append("circle")
	.attr("cx", 50)
	.attr("cy", 50)
	.attr("opacity", 1)
	.attr("r", 25);

circle.transition()
	//.delay(1000) //delay before transition, and each(start)
	.duration(1000)
	.attr("cx", 150)
	.transition()
	.attr("opacity", .2) //change the opacity
	.each("start", function() {//at the start/end of the last transition
		d3.select(this) //this is the circle
		.attr("fill","red");
	})