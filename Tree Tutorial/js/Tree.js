//ref
//https://www.youtube.com/watch?v=iZ6MSHA4FMU

/////////////////////variables//////////////////////////
//canvas
var canvas;
var iCANVAS_WIDTH = 768;
var iCANVAS_HEIGHT = 768;

///////////////////////functions////////////////////////


///////////////////////main////////////////////////


//create canvas
canvas = d3.select("body")
	.append("svg")
	.attr("width", iCANVAS_WIDTH)
	.attr("height", iCANVAS_HEIGHT)
	.append("g")
	.attr("transform", "translate(50,50)");

//tree example
var diagonalExp = d3.svg.diagonal()
	.source({
		x: 50,
		y: 50
	})
	.target({
		x: 100,
		y: 190
	});

canvas.append("path")
	.attr("fill", "none")
	.attr("stroke", "blue")
	.attr("d", diagonalExp);

var tree = d3.layout.tree()
	.size([400, 400]);

d3.json("data/tree.json", function(data) {
	var nodes = tree.nodes(data); //run tree layout, return all the objects
	var links = tree.links(nodes);

	var node = canvas.selectAll(".node") //there's no node currently
		.data(nodes)
		.enter() //for each node
		.append("g")
		.attr("class", "node")
		.attr("transform", function(d) {
			return "translate(" + d.y + "," + d.x + ")";//rotate to horizontal
		})

	node.append("circle")
		.attr("r", 5)
		.attr("fill", "steelblue");

	node.append("text")
		.text(function(d) {
			return d.name;
		});

	
	var diagonal = d3.svg.diagonal()
	.projection(function(d) {return [d.y, d.x];}) //start and end points
	
	canvas.selectAll(".link")
		.data(links)
		.enter()
		.append("path")
		.attr("class", "link")
		.attr("fill", "none")
		.attr("stroke", "#ADADAD") //light grey
		.attr("d", diagonal);

})