/////////////////////variables//////////////////////////
//canvas
var canvas;
var iCANVAS_WIDTH = 768;
var iCANVAS_HEIGHT = 768;

///////////////////////functions////////////////////////


///////////////////////main////////////////////////


//create canvas
canvas = d3.select("body")
	.append("svg")
	.attr("width", iCANVAS_WIDTH)
	.attr("height", iCANVAS_HEIGHT)
	.append("g")
	.attr("transform", "translate(50,50)");

var pack = d3.layout.pack()
	.size([iCANVAS_WIDTH, iCANVAS_HEIGHT - 50])
	.padding(10); //??

d3.json("data/cluster.json", function(data) {
	var nodes = pack.nodes(data); //generating the nodes data for pack
	console.log(nodes);

	var node = canvas.selectAll(".node")
		.data(nodes)
		.enter()
		.append("g")
		.attr("class", "node")
		.attr("transform", function(d) {
			return "translate(" + d.x + "," + d.y + ")"; //rotate to horizontal
		})

	node.append("circle")
		.attr("r", function(d) {
			return d.r;
		}) //???
		.attr("fill", "steelblue")
		.attr("opacity", 0.25)
		.attr("stroke", "#ADADAD")
		.attr("stroke-width", 2);

	node.append("text")
		.text(function(d) {
			return d.children ? "" : d.name;
		});


})